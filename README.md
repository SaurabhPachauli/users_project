## Steps to run this project:

1) Install node in your system if you don't have it already installed.
2) Install node package dependencies, using "npm i" command.
3) Install angular cli using command "npm i @angular/cli".
4) Start local JSON server using command "npx json-server --watch server/db.json".
5) Serve angular project with command "ng serve" or "ng s".
6) Use following login credentials to acces the user module.
    
    username: 'test@mcu.com'
    password: 'p@ssw0rd'


## Folder organization and project details:

1) The entire project has been broadly divided into two primary module types:
    a) Secure Modules --> The ones that require authenticated access.
    b) Non-secure Modules --> The ones that require no authentication for access, and are available for anonymous viewing.
2) These modules are further broken into different blocks/modules, dependent components being placed under one sub-module.
3) These sub-modules are further broken into:
    a) Components: Housing all the dumb components(without API calls).
    b) Pages: Housing smart components(with API calls).
4) All of the style is placed under "styles" folder, for better management.
5) Secure modules use the AuthGuardService to grant access to logged-in users.