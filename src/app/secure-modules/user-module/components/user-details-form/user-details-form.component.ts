import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-details-form',
  templateUrl: './user-details-form.component.html'
})
export class UserDetailsFormComponent implements OnInit {

  @Input() userDetails;
  @Output() userData = new EventEmitter();
  @Output() closeDialog = new EventEmitter();
  userDetailsForm: FormGroup

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userDetailsForm = this.formBuilder.group({
      id: [this.userDetails ? this.userDetails.id : ''],
      firstName: [this.userDetails ? this.userDetails.firstName : '', [Validators.required]],
      lastName: [this.userDetails ? this.userDetails.lastName : '', [Validators.required]],
      phone: [this.userDetails ? this.userDetails.phone : '', [Validators.required]],
      email: [this.userDetails ? this.userDetails.email : '', [Validators.required]],
      status: ['Active']
    });
  }

  get f() {
    return this.userDetailsForm.controls;
  }

  onSave(data) {
    this.userData.emit(data);
  }

  onClose() {
    this.closeDialog.emit(false);
  }

}
