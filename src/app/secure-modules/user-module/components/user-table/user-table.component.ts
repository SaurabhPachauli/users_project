import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html'
})
export class UserTableComponent implements OnInit {

  cols = [];
  @Input() users: [];
  @Output() editUserData = new EventEmitter();
  @Output() activateDiactivateUser = new EventEmitter();
  constructor(private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'firstName', header: 'First Name' },
      { field: 'lastName', header: 'Last Name' },
      { field: 'email', header: 'E-Mail' },
      { field: 'phone', header: 'Phone' },
      { field: 'status', header: 'status' }
    ];
  }

  editUser(user) {
    this.editUserData.emit(user);
  }

  disableUser(user) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to deactivate ${user.firstName + ' ' + user.lastName}?`,
      accept: () => {
        user.status = "Inactive";
        this.activateDiactivateUser.emit(user);
      }
    });
  }

  activateUser(user) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to Activate ${user.firstName + ' ' + user.lastName}?`,
      accept: () => {
        user.status = "Active";
        this.activateDiactivateUser.emit(user);
      }
    });
  }

}
