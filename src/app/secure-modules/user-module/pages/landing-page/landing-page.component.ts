import { Component, OnInit } from '@angular/core';
import { CommonServicesService } from 'src/app/secure-modules/shared/services/common-services.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html'
})
export class LandingPageComponent implements OnInit {

  users = [];
  display = false;
  userDetails;

  constructor(private commonService: CommonServicesService, ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.commonService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  createUser() {
    this.userDetails = null;
    this.display = true;
  }

  editUserData(event) {
    this.display = true;
    this.userDetails = event;
  }

  closeDialog(event) {
    this.display = event;
  }

  saveData(event) {
    const data = JSON.parse(JSON.stringify(event));
    if (event.id) {
      const id = event.id;
      delete data.id;
      this.commonService.updateUser(id, data).subscribe(response => {
        this.getUsers();
      });
    } else {
      this.commonService.createUser(data).subscribe(response => {
        this.getUsers();
      });
    }
    this.display = false;
  }

  activateDiactivateUser(event) {
    const data = JSON.parse(JSON.stringify(event));
    const id = event.id;
    delete data.id;
    this.commonService.updateUser(id, data).subscribe(response => {
      this.getUsers();
    });
  }

}
