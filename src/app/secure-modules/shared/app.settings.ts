import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment';

@Injectable()
export class AppSettings {
    public static BASE_USRL = environment.baseUrl;
    public static LOGIN_DETAILS = {
        username: 'test@mcu.com',
        password: 'p@ssw0rd',
        token: 'authenticated'
    }
}