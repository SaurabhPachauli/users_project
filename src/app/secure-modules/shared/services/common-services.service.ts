import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { RestServicesService } from './rest-services.service';

@Injectable({
  providedIn: 'root'
})
export class CommonServicesService {

  constructor(private restService: RestServicesService) { }

  getUsers(): Observable<any> {
    return this.restService.fetch('employees');
  }

  createUser(data): Observable<any> {
    return this.restService.create(`employees`, data);
  }

  updateUser(id, data): Observable<any> {
    return this.restService.update(`employees/${id}`, data);
  }

}
