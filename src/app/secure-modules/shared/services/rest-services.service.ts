import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AppSettings } from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class RestServicesService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  fetch(url): Observable<any> {
    return this.http.get<any>(AppSettings.BASE_USRL + url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  create(url, data): Observable<any> {
    return this.http.post<any>(AppSettings.BASE_USRL + url, data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  update(url, data?): Observable<any> {
    return this.http.put<any>(AppSettings.BASE_USRL + url, data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  delete(url) {
    return this.http.delete<any>(AppSettings.BASE_USRL + url, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
