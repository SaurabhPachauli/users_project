import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonServicesService } from '../../secure-modules/shared/services/common-services.service';
import { AppSettings } from 'src/app/secure-modules/shared/app.settings';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html'
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private route: Router) { }

  ngOnInit(): void {
    this.checkIfLoggedIn();
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  checkIfLoggedIn() {
    if (localStorage.getItem('token') === AppSettings.LOGIN_DETAILS.token) {
      this.route.navigate(['users']);
    }
  }

  get f() {
    return this.loginForm.controls;
  }

  login(formData) {
    if (formData.username === AppSettings.LOGIN_DETAILS.username && formData.password === AppSettings.LOGIN_DETAILS.password) {
      localStorage.setItem('token', AppSettings.LOGIN_DETAILS.token);
      this.route.navigate(['users']);
    } else {
      alert('Username or password Invalid!');
      this.loginForm.reset();
    }
  }



}
